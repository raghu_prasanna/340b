﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace AspCares.Common.Core
{
    public class GridModel
    {
        public List<GridHeader> Header { get; set; }
        public List<GridRow> Rows { get; set; }
    }

    public class GridHeader
    {
        public bool PrimaryKey { get; set; }
        public string DataField { get; set; }
        public string DisplayName { get; set; }
        public string ColumnType { get; set; }
        public int ColumnWidth { get; set; }
    }

    public class GridRow
    {
        public List<Dictionary<object,object>> Cells { get; set; }
    }

    public class GridConstruct
    {
        private static ArrayList arlHiddenFileds = new ArrayList();
        public static GridModel FormGridModel(DataTable dtbGridData, string PrimaryKeyColumn)
        {
            GridModel objGridModel = new GridModel();
            
            
            List<GridHeader> objHeaders = new List<GridHeader>();
            foreach (DataColumn dtcHeader in dtbGridData.Columns)
            {
                GridHeader objHeader = new GridHeader();
                objHeader.DataField = dtcHeader.ColumnName;
                objHeader.DisplayName = dtcHeader.ColumnName;


                

                objHeader.ColumnWidth = 250;
                objHeader.PrimaryKey = false;
                if (dtcHeader.ColumnName == PrimaryKeyColumn)
                {
                    objHeader.ColumnType = "Hidden";
                    objHeader.PrimaryKey = true;
                }
                else if (dtcHeader.DataType.Name == "DateTime")
                    objHeader.ColumnType = "DateTime";
                else if (dtcHeader.DataType.Name == "TimeSpan")
                    objHeader.ColumnType = "TimeSpan";
                else
                    objHeader.ColumnType = "Label";

                if (HiddenFieldCollection.Count > 0)
                {
                    foreach (string strColumnName in HiddenFieldCollection)
                    {
                        if (strColumnName.ToUpper() != PrimaryKeyColumn.ToUpper() && dtcHeader.ColumnName.ToUpper() == strColumnName.ToUpper())
                        {
                            objHeader.ColumnType = "Hidden";
                        }
                    }
                }
                objHeaders.Add(objHeader);
            }
            objGridModel.Header = objHeaders;

            List<GridRow> objRows = new List<GridRow>();
            foreach (DataRow dtrRow in dtbGridData.Rows)
            {
                GridRow objSingleRow = new GridRow();
                List<Dictionary<object, object>> objCellValues = new List<Dictionary<object, object>>();
                foreach (DataColumn dtcHeader in dtbGridData.Columns)
                {
                    Dictionary<object, object> objCell = new Dictionary<object, object>();
                    objCell.Add(dtcHeader.ColumnName, dtrRow[dtcHeader.ColumnName]);
                    objCellValues.Add(objCell);
                }
                objSingleRow.Cells = objCellValues;
                objRows.Add(objSingleRow);
            }
            objGridModel.Rows = objRows;
            return objGridModel;
        }
        public static ArrayList HiddenFieldCollection
        {
            get { return arlHiddenFileds; }
            set { arlHiddenFileds = value; }
        }

        public static GridModel FormGridModel<T>(List<T> objGridDataList, string PrimaryKeyColumn)
        {
            DataTable dtbData = ListToDataTable(objGridDataList);
            return FormGridModel(dtbData, PrimaryKeyColumn);
        }

        public static DataTable ListToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                if (prop.PropertyType.ToString().Contains("DateTime"))
                    dataTable.Columns.Add(prop.Name, Type.GetType("System.DateTime"));
                else if (prop.PropertyType.ToString().Contains("TimeSpan"))
                    dataTable.Columns.Add(prop.Name, Type.GetType("System.TimeSpan"));
                else if (prop.PropertyType.ToString().Contains("Guid"))
                    dataTable.Columns.Add(prop.Name, Type.GetType("System.Guid"));
                else if (prop.PropertyType.ToString().Contains("Int"))
                    dataTable.Columns.Add(prop.Name, Type.GetType("System.Int32"));
                else
                    dataTable.Columns.Add(prop.Name, prop.PropertyType);

            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}