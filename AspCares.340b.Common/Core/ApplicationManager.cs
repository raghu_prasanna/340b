﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCares.Common.Core
{
    public class ApplicationManager
    {
        public string BaseUrl { get; set; }

        public int LanguageId { get; set; }        

        public ApplicationManager()
        {
            //this.BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        }

        public string ClientTimeZone { get; set; }
    }
}
