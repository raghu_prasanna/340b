﻿using AspCares.ServiceModel;
using ASPCares.DataAccess;
using ASPCares.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ASPCares.BusinessLogic
{
    public class User
    {        
        public User(string str)
        {            
        }

        public enum Role { SuperAdmin = 1, Administrator = 2, Operation = 3, Client340b = 4, Sales = 5, ContractedPharmacy = 6 };

        public ASpCaresServiceResponse RegisterUser(object objParams)
        {
            ASpCaresServiceResponse objServiceResponseData = new ASpCaresServiceResponse();
            Dictionary<string, object> objMParams = objParams as Dictionary<string, object>;
            UserModel objUserModel = new UserModel();
            objUserModel.vFirstName = objMParams["vFirstName"].ToString();
            objUserModel.vLastName = objMParams["vLastName"].ToString();
            objUserModel.vUserName = objMParams["vUserName"].ToString();
            objUserModel.vPassword = objMParams["vPassword"].ToString();
            objUserModel.vEmail = objMParams["vEmail"].ToString();
            objUserModel.vCompany = objMParams["vCompany"].ToString();
            objUserModel.vAddress = objMParams["vAddress"].ToString();
            objUserModel.iRoleId = Convert.ToInt32(objMParams["iRoleId"].ToString());
            string strUserXML = FormUserXMl(objUserModel);
            
            AspCareDataAccess objDataAccess = new AspCareDataAccess();
            Hashtable htbUser = new Hashtable();
            htbUser.Add("@vUserXML", strUserXML);
            htbUser.Add("@uUserId", Guid.Empty.ToString());
            DataSet dstUser = objDataAccess.ExecuteProcedure("Proc_ASPCares_InsertUser",htbUser);
            objUserModel.UserId = new Guid( dstUser.Tables[0].Rows[0]["uUserId"].ToString());
            objServiceResponseData.Data = objUserModel;
            return objServiceResponseData;
        }

        private string FormUserXMl(UserModel objUserModel)
        {
            StringBuilder stbUser = new StringBuilder();
            stbUser.Append("<User>");
            stbUser.Append("<vFirstName>"+ objUserModel.vFirstName + "</vFirstName>");
            stbUser.Append("<vLastName>" + objUserModel.vLastName + "</vLastName>");
            stbUser.Append("<vUserName>" + objUserModel.vUserName + "</vUserName>");
            stbUser.Append("<vPassword>" + objUserModel.vPassword + "</vPassword>");
            stbUser.Append("<vEmail>" + objUserModel.vEmail + "</vEmail>");
            stbUser.Append("<vCompany>" + objUserModel.vCompany + "</vCompany>");
            stbUser.Append("<vAddress>" + objUserModel.vAddress + "</vAddress>");
            stbUser.Append("<iRoleId>" + objUserModel.iRoleId + "</iRoleId>");
            stbUser.Append("</User>");
            return stbUser.ToString();
        }
    }
}
