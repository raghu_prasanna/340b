﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPCares.DataAccess
{
    public class AspCareDataAccess
    {
        public AspCareDataAccess()
        {
        }
        public DataSet ExecuteProcedure(string strProcedureName, Hashtable htbInputParamCollection)
        {
            DataSet dstData = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(@"server=AXBAN-RAGHU-2\SQLDB01;database=AspCares;uid=sa;pwd=qwer,123"))
                {
                    using (SqlCommand cmd = new SqlCommand(strProcedureName, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (htbInputParamCollection != null && htbInputParamCollection.Count > 0)
                        {
                            foreach (string strKey in htbInputParamCollection.Keys)
                            {
                                cmd.Parameters.AddWithValue(strKey, htbInputParamCollection[strKey]);
                            }
                        }
                        SqlDataAdapter objMySqlDataAdapter = new SqlDataAdapter(cmd);
                        objMySqlDataAdapter.Fill(dstData);
                        con.Dispose();
                        con.Open();
                        cmd.ExecuteReader();
                    }
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                GC.Collect();
            }
            return dstData;
        }
    }
}
