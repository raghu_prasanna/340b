﻿using System;

namespace ASPCares.DataModel
{
    public class UserModel
    {
        public string vFirstName { set; get; }
        public string vLastName { set; get; }
        public string vEmail { set; get; }
        public string vPassword { set; get; }
        public string vUserName { set; get; }
        public string vCompany { set; get; }
        public int iRoleId { set; get; }
        public string vAddress { set; get; }

        public Guid UserId { set; get; }
    }
}
