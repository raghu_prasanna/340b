﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCares.ServiceModel
{    
    public class ASpCaresServiceParams
    {
        public string TargetClass { get; set; }
        public string TargetMethod { get; set; }
        public Dictionary<string, object> Parameter { get; set; }
    }

    public class ASpCaresServiceResponse
    {
        public string TargetClass { get; set; }
        public object Data { get; set; }

    }
}
