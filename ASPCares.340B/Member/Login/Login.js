﻿var objAppManager = new Object();
objAppManager.BaseUrl = "http://localhost/AspCaresAPI/";
$(document).ready(function () {
    $("#btnSignUp").click(function (e) {
        MemberRegistration();
    });
    $("#btnSignIn").click(function (e) {
        LoginUser();
    });
});

function MemberRegistration() {
    var objMemberDetails = {
        vFirstName: $("#txtFirstName").val(),
        vLastName: $("#txtLastName").val(),
        vUserName: $("#txtUserName").val(),
        vPassword: $("#txtPassword").val(),
        vEmail: $("#txtEmail").val(),
        iRoleId: 1,
        vAddress: $("#txtAddress").val(),
        vCompany: $("#txtCompamy").val()
    };

    var objServiceParams = {
        TargetClass: 'ASPCares.BusinessLogic.User,ASPCares.340B.BusinessLogic',
        TargetMethod: 'RegisterUser',
        Parameter: objMemberDetails,
        CallbackMethod: function (objServiceResponse) {
            console.log(objServiceResponse);
            //bootbox.hideAll();
            //if (objServiceResponse.uMemberId != undefined && objServiceResponse.uMemberId != null) {
            //    bootbox.dialog({ message: "Your username and Password has been set to you registered Email", title: "Registration Successfull", buttons: { success: { label: "Ok", className: "btn-success" } } });
            //}
            //else {
            //    bootbox.dialog({ message: "User Already Exists with same Email and UserName", title: "User Already Exists", buttons: { success: { label: "Ok", className: "btn-success" } } });
            //}
        }
    };
    AspCares_ServiceInvoker(objServiceParams);
}