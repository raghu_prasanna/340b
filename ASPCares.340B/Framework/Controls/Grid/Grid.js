﻿(function ($) {
    if ($("head").find("#JGridCSS").length <= 0)
        $("<link id='JGridCSS' rel='stylesheet' type='text/css' href='" + objAppManager.BaseUrl + "App_Themes/Default/Css/Controls/Grid.css" + "' />").appendTo("head");
    $.fn.Grid = function (objParams) {
        var objCurrentObject = $(this);
        var defaults = {
            GridDataJson: null,
            OnLoadComplete: null,
            GridBaseCss: 'JGrid',
            ShowCheckboxColumn: false
        };

        var objExtendedParams = $.extend(defaults, objParams);
        $.BuildGrid(objCurrentObject, objExtendedParams);
    };

    $.BuildGrid = function (objCurrentObject, objExtendedParams) {

        if (objExtendedParams.GridDataJson != null) {
            var objGridData = objExtendedParams.GridDataJson;
            var blnIsGridRowsPresent = false;
            if (objGridData.Header != null && objGridData.Header != undefined) {
                //var intHeaderColumn = $.GetMaxHeaderColumnCount(objGridData.Header);
                var strGridTable = "<table id='JQueryGrid' class='" + objExtendedParams.GridBaseCss + "' cellpadding='0' cellspacing='0' border='0' width='100%' height='100%'>";
                /************** Build Grid Header **************************/
                strGridTable += "<tr Type='GridHeader' class='HeaderRow'>";
                var strPrimaryKeyName = "";
                if (objExtendedParams.ShowCheckboxColumn) strGridTable += "<td class='HeaderCell' style='width:20px;'><input type='checkbox' id='chkHeader' align='left' IsHeaderCheckbox='Y' /></td>";
                $.each(objGridData.Header, function (intIndex, objHeaderColumn) {
                    if (objHeaderColumn.PrimaryKey) strPrimaryKeyName = objHeaderColumn.DataField;
                    if (objHeaderColumn.ColumnType.toUpperCase() != "HIDDEN") {
                        var strWidth = "";
                        var strHeaderClass = "class='HeaderCell_Last'";
                        if (intIndex != objGridData.Header.length - 1) {
                            var strWidth = "style=width:" + objHeaderColumn.ColumnWidth + "px";
                            strHeaderClass = "class='HeaderCell'";
                        }
                        strGridTable += "<td " + strWidth + " " + strHeaderClass + ">" + objHeaderColumn.DisplayName + "</td>";
                    }
                });
                /***********************************************************/
                
                if (objGridData.Rows.length > 0) {
                    blnIsGridRowsPresent = true;
                    $.each(objGridData.Rows, function (intRowIndex, objRow) {
                        var srGridCellHtml = "";
                        var strHiddenAttributes = "";
                        if (objExtendedParams.ShowCheckboxColumn) srGridCellHtml += "<td class='DataCell' style='width:20px;'><input type='checkbox' align='left' SelectionCheckbox='Y' /></td>";
                        $.each(objRow.Cells, function (intColIndex, objCellData) {
                            var strCellValue = "";
                            for (var strKey in objCellData) {
                                strCellValue = objCellData[strKey]
                            }
                            if (objGridData.Header[intColIndex].ColumnType.toUpperCase() != "HIDDEN") {
                                var strWidth = "";
                                var strDataCellClass = "class='DataCell_Last'";
                                if (intColIndex != objGridData.Header.length - 1) {
                                    var strWidth = "style=width:120px";
                                    strDataCellClass = "class='DataCell'";
                                }
                                if (strCellValue == "Y") strCellValue = "<img src='" + objAppManager.BaseUrl + "App_Themes/Default/Images/Common/Checked.png" + "'>";
                                else if (strCellValue == "N") strCellValue = "<img src='" + objAppManager.BaseUrl + "App_Themes/Default/Images/Common/UnChecked.png" + "'>";
                                else if (strCellValue == null || strCellValue == "null") strCellValue = "";
                                if (objGridData.Header[intColIndex].ColumnType.toUpperCase() == "DATETIME") {
                                    if (strCellValue != "")
                                        srGridCellHtml += "<td " + strWidth + " " + strDataCellClass + ">" + GetCultureDate(strCellValue) + "</td>";
                                    else srGridCellHtml += "<td " + strWidth + " " + strDataCellClass + ">" + strCellValue + "</td>";
                                }
                                else if (objGridData.Header[intColIndex].ColumnType.toUpperCase() == "TIMESPAN") {
                                    if (strCellValue != "")
                                        srGridCellHtml += "<td " + strWidth + " " + strDataCellClass + ">" + GetCultureTime(strCellValue) + "</td>";
                                    else srGridCellHtml += "<td " + strWidth + " " + strDataCellClass + ">" + strCellValue + "</td>";
                                }
                                else
                                    srGridCellHtml += "<td " + strWidth + " " + strDataCellClass + ">" + strCellValue + "</td>";
                            }
                            else {
                                for (var strKey in objCellData) {
                                    strHiddenAttributes += " " + strKey + "='" + strCellValue + "'";
                                }
                            }
                        });
                        strGridTable += "<tr Type='GridDataRow' class='GridRow' " + strHiddenAttributes + ">" + srGridCellHtml + "</tr>";
                    });
                }
                else {
                    var intColspan = 1;
                    intColspan = objGridData.Header.length;
                    if (objExtendedParams.ShowCheckboxColumn) {
                        intColspan = intColspan + 1;
                    }
                    strGridTable += "<tr><td  style='height:10px;' colspan='" + intColspan + "'><div GridNoDataRowSpacer='Y' ></td></tr>";
                    strGridTable += "<tr><td align='center' colspan='" + intColspan + "'><div GridNoDataRow='Y' class='bb-alert alert alert-info' style='display:none;'>";
                    strGridTable += "<span>The examples populate this alert with dummy content</span>";
                    strGridTable += "</div></td></tr>";
                }
                strGridTable += "</tr>";
                strGridTable += "</table>";
                $(objCurrentObject).html(strGridTable);
                if (!blnIsGridRowsPresent) {
                    $(function () {
                        Example.init({
                            "selector": "[GridNoDataRow]"
                        });
                    });
                    Example.show("No Results to view", false);
                }
            }
        }
        if (objExtendedParams.OnLoadComplete != null) objExtendedParams.OnLoadComplete();
    };

    $.GetMaxHeaderColumnCount = function (arrHeader) {
        var intHeaderCount = 0;
        $.each(arrHeader, function (intIndex, objHeader) {
            if (objHeader.ColumnType.toUpperCase() != "HIDDEN") {
                intHeaderCount + 1;
            }
        });
        return intHeaderCount;
    }

    $.fn.GetSelectedRow = function () {
        var objCurrentObject = $(this);
        var objSelectedRow = null;
        if ($(objCurrentObject.length > 0 && $(objCurrentObject).find("#JQueryGrid").length > 0)) {
            var objArrRows = $(objCurrentObject).find("#JQueryGrid").children().children();
            if ($(objArrRows).length > 0) {
                if ($(objArrRows).find("[SelectionCheckbox='Y']:checked").length > 0) {
                    objSelectedRow = $(objArrRows).find("[SelectionCheckbox='Y']:checked").first().closest("tr[Type='GridDataRow']");
                }
            }
        }
        return objSelectedRow;
    };

    $.fn.GetAllCheckedItems = function () {
        var objCurrentObject = $(this);
        var arrSelectedRows = new Array();
        if ($(objCurrentObject.length > 0 && $(objCurrentObject).find("#JQueryGrid").length > 0)) {
            var objArrRows = $(objCurrentObject).find("#JQueryGrid").children().children();
            if ($(objArrRows).length > 0) {
                if ($(objArrRows).find("[SelectionCheckbox='Y']:checked").length > 0) {
                    $.each($(objArrRows).find("[SelectionCheckbox='Y']:checked"), function (intIndex, objCheckbox) {
                        var objGridRow = $(objCheckbox).closest("tr[Type='GridDataRow']");
                        arrSelectedRows.push(objGridRow);
                    });
                }
            }
        }
        return arrSelectedRows;
    };
}(jQuery));