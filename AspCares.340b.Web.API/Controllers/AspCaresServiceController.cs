﻿using AspCares.ServiceModel;
using System;
using System.Reflection;
using System.Web.Http;

namespace ASPCares._340B.Web.API.Controllers
{
    public class AspCaresServiceController : ApiController
    {

        [HttpPost]
        public ASpCaresServiceResponse FetchData(ASpCaresServiceParams objServiceData)
        {
            //ApplicationManager objAppManager = HttpContext.Current.Session[Utility.SessionKey + "AppManager"] as ApplicationManager;
            ASpCaresServiceResponse objResponse = null;
            try
            {

                object objInstance = Activator.CreateInstance(Type.GetType(objServiceData.TargetClass), new object[] { string.Empty });
                Type objType = objInstance.GetType();
                MethodInfo objMethodPointer = objType.GetMethod(objServiceData.TargetMethod);
                objResponse = (ASpCaresServiceResponse)objMethodPointer.Invoke(objInstance, new object[] { objServiceData.Parameter });
                if (objResponse != null)
                    objResponse.TargetClass = objServiceData.TargetClass;
                else
                    objResponse = new ASpCaresServiceResponse();
            }
            catch (Exception objError)
            {
                var strActualMsg = string.Empty;
                if (objError.InnerException != null)
                {
                    strActualMsg = "Message: " + objError.InnerException.Message + "---- Desc: " + objError.InnerException.StackTrace;
                    //ErrorLogger.LogException(objError.InnerException.Message, objError.InnerException.StackTrace, 1);
                }
                else
                {
                    strActualMsg = "Message: " + objError.Message + "---- Desc: " + objError.StackTrace;
                    //ErrorLogger.LogException(objError.Message, objError.StackTrace, 1);
                }


                HttpError objHttpError = new HttpError(strActualMsg);
                if (objResponse == null)
                {
                    objResponse = new ASpCaresServiceResponse();
                    objResponse.Data = strActualMsg;
                }
                //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, objHttpError));

            }
            return objResponse;
        }
    }
}